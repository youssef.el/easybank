import React from 'react'
import Header from './components/Header'
import Hero from './components/Hero'
import WhyUs from './components/WhyUs'
import Blog from './components/Blog'
import Footer from './components/Footer'

const App = () => {
  return (
    <div>
      <Header />
      <Hero />
      <WhyUs />
      <Blog />
      <Footer />
    </div>
  )
}

export default App
