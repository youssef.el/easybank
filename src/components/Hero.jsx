import React from 'react'
import img from '../assets/images/bg-intro-desktop.svg'
import img2 from '../assets/images/image-mockups.png'

const Hero = () => {
  return (
    <div className='relative z-10 flex flex-col-reverse py-6 mt-[-200px] md:mt-[-180px] md:py-0 md:flex-row pl-6 md:pl-28 mx-auto items-center justify-between bg-gray-100'>
      <div className='flex flex-col w-5/6 mx-auto text-center md:text-left md:w-1/3 lg:space-y-8 '>
        <h2 className='text-4xl font-bold text-primary'>Next generation digital banking</h2>
        <p className='text-secondary'>Lorem ipsum dolor sit amet consectetur adipisicing elit.
           Quasi cum sequi ipsam illum eius aut accusamus, sit alias
            dolorem amet?
        </p>
        <div>
          <button className='lg:inline-block btn rounded-full px-10 text-md normal-case text-white bg-gradient-to-r from-green-400 to-blue-300 border-none '>Request Invite</button>
        </div>
      </div>
      <div className='relative w-[220%] md:w-3/4' >
        <img src={img} className='absolute -z-10 top-0 translate-x-[100px] w-[100%] translate-y-[-80px] md:translate-x-[100px] md:translate-y-[-5px] left-0  h-full '  />
        <img src={img2} className='mx-auto top-0 w-[45%] translate-y-[-19px] md:translate-x-[80px] left-0 md:w-full h-1/3 md:translate-y-[120px]'/>
      </div>
    </div>
  )
}

export default Hero
