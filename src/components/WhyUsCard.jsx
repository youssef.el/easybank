import React from 'react'

const WhyUsCard = (props) => {
  return (
    <div className='flex flex-col text-center mt-10 space-y-8 items-center md:items-start md:text-left'>
        
        <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
          <defs>
            <linearGradient id="a" x1="0%" x2="99.58%" y1="0%" y2="99.58%">
              <stop offset="0%" stop-color="#33D35E"/>
              <stop offset="100%" stop-color="#2AB6D9"/>
              </linearGradient>
          </defs>
          <g fill="none" fill-rule="evenodd">
            <circle cx="36" cy="36" r="36" fill="url(#a)"/>
            <path fill="#FFF" fill-rule="nonzero" 
            d={props.svg}/>
            </g>
        </svg>
        <h4 className='font-semibold text-2xl'>{props.title}</h4>
        <p className='text-secondary'>{props.text}</p>
      </div>
  )
}

export default WhyUsCard
