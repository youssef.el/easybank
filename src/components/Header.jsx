import React, { useState } from 'react'
import logo from '../assets/images/logo.svg'
import {AiOutlineMenu, AiOutlineClose} from 'react-icons/ai'


const Header = () => {

  const [open, setOpen] = useState(false)
  console.log(open)
  return (
    <div className='relative z-20 bg-white'>
      <div className='container p-6 mx-auto flex justify-between items-center'>
        {/* logo */}
        <img src={logo} alt="" />
        {/* menu links */}
        <div className='hidden lg:flex space-x-3 text-secondary mx-auto'>
          <a href="">Home</a>
          <a href="">About</a>
          <a href="">Contact</a>
          <a href="">Blog</a>
          <a href="">Carrers</a>
        </div>
        <div>
          <button className='hidden lg:inline-block btn rounded-full px-10 text-md normal-case text-white bg-gradient-to-r from-green-400 to-blue-300 border-none '>Request Invite</button>
        </div>
      {/* humburger menu */}
        <label className="btn btn-circle bg-transparent border-none swap swap-rotate hover:bg-transparent lg:hidden"> 
            <input type="checkbox" />
            {!open && <AiOutlineMenu size={30} onClick={()=> setOpen(true)} className='swap-off  text-black'/>}
            {open && <AiOutlineClose size={30} onClick={()=> setOpen(false)} className='swap-on  text-black'/>}     
        </label> 
      </div>
      <div className={open ? 'flex flex-col items-center bg-white mx-10 mt-4 font-semibold rounded-xl py-4 space-y-4 text-primary lg:hidden' : 'hidden'} >
          <a href="">Home</a>
          <a href="">About</a>
          <a href="">Contact</a>
          <a href="">Blog</a>
          <a href="">Carrers</a>
        </div>
    </div>

  )
}

export default Header
