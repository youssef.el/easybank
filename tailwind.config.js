/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {}
  },
  daisyui: {
    themes: [
      {
        mytheme: {
        
                "primary": "#374151",
                          
                "secondary": "#9ca3af",
                          
                "accent": "#37CDBE",
                          
                "neutral": "#3D4451",
                          
                "base-100": "#FFFFFF",
                          
                "info": "#3ABFF8",
                          
                "success": "#36D399",
                          
                "warning": "#FBBD23",
                          
                "error": "#F87272",
        },
      },
    ],
  },
  plugins: [require("daisyui")],
}

